import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TableService{
    constructor(private http: HttpClient) {
    }

    getByLocation(location:string, pagination :number) : Observable<any>{
        return this.http.get<any>(`https://api.github.com/search/users?q=location:${location}&page=${pagination}&per_page=5`)
    }
}