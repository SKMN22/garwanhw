import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TagContentType } from '@angular/compiler';
import { TableService } from 'src/app/service/service';

@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.scss']
})
export class TableComponentComponent implements OnInit {


  public inputValue:string = "";
  public response:any[] = [];

  constructor() { }

  ngOnInit() {
  }

  onKeyUpHandler(event : any){
    this.inputValue = event.target.value;
  }

}
