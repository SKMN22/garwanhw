import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSpecificColumnComponent } from './table-specific-column.component';

describe('TableSpecificColumnComponent', () => {
  let component: TableSpecificColumnComponent;
  let fixture: ComponentFixture<TableSpecificColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSpecificColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSpecificColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
