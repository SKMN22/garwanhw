import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-table-specific-column',
  templateUrl: './table-specific-column.component.html',
  styleUrls: ['./table-specific-column.component.scss']
})
export class TableSpecificColumnComponent implements OnInit {


  @Input('url')
  public url:string;

  public number:number ;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    // get number of repos/followers based on url
    this.http.get(this.url).subscribe(
      (data: any[]) =>{
        if(data)
          this.number = data.length;
      }
    )
  }

}
