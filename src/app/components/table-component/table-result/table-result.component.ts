import { Component, OnInit, Input, ViewChild, OnChanges, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { TableResultState, GetByLocationAndPagination, getLoading, getData } from 'src/app/store';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { TableDialogWindowComponent } from './table-dialog-window/table-dialog-window.component';


@Component({
  selector: 'app-table-result',
  templateUrl: './table-result.component.html',
  styleUrls: ['./table-result.component.scss']
})
export class TableResultComponent implements OnInit, OnChanges, AfterViewInit {


  @Input('location')
  public location: string;

  public paginationNumber: number = 1;
  public data: any[] = [];
  public pageStart: number = 1;
  public pageEnd: number = 5;
  public total: number = 0;
  public descending: boolean = false;

  public data$: Observable<any>;
  public loading$: Observable<boolean>;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['position', 'name', 'repos', 'followers', 'images'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private store: Store<TableResultState>, private http: HttpClient, private dialog: MatDialog) {
    this.loading$ = this.store.select(getLoading);
    this.data$ = this.store.select(getData);
  }

  ngOnInit() {

  }

  ngOnChanges() {

    this.paginationNumber = 1;
    this.pageStart = 1;
    this.pageEnd = 5;
    console.log(this.location);
    this.store.dispatch(new GetByLocationAndPagination(this.location, this.paginationNumber))
    this.subscribeData()
  }

  ngAfterViewInit() {

  }

  // function for subscibing data
  subscribeData() {
    this.data$.subscribe(
      data => {
        if (data.items) {
          this.data = data.items
          this.total = data.total_count;
          if (this.total < this.pageEnd && this.total != 0) {
            this.pageEnd = this.total;
          }
          this.dataSource = new MatTableDataSource(this.data);
        }
      }
    )
  }

  // load previous page
  handlePrevious() {
    if (this.pageStart < 5)
      return;
    else {
      this.pageStart = this.pageStart - 5;
      this.pageEnd = this.pageEnd - 5;
      this.paginationNumber = this.paginationNumber - 1;
      this.store.dispatch(new GetByLocationAndPagination(this.location, this.paginationNumber))
    }
  }

  // load next page
  handleNext() {
    if ((this.pageStart + 5) > this.total)
      return;
    else {
      this.pageStart = this.pageStart + 5;
      this.pageEnd = this.pageEnd + 5;
      this.paginationNumber = this.paginationNumber + 1;
      this.store.dispatch(new GetByLocationAndPagination(this.location, this.paginationNumber))
    }
  }

  // opening dialog window after click
  openDialogInfo(element: any) {
    console.log(element)
    const dialogRef = this.dialog.open(TableDialogWindowComponent,
      {
        width: '250px',
        data: { element: element }
      })
  }

  // sort by name - all first switched to lowerCase
  sortByName() {
    if (this.descending) {
      this.data.sort(function (a, b) { return (a.login.toLowerCase() > b.login.toLowerCase()) ? 1 : ((b.login.toLowerCase() > a.login.toLowerCase()) ? -1 : 0); });
      this.dataSource = new MatTableDataSource(this.data);
      this.descending = !this.descending;
    }
    else{
      this.data.sort(function (a, b) { return (a.login.toLowerCase() < b.login.toLowerCase()) ? 1 : ((b.login.toLowerCase() < a.login.toLowerCase()) ? -1 : 0); });
      this.dataSource = new MatTableDataSource(this.data);
      this.descending = !this.descending;
    }
  }

}
