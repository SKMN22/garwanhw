import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDialogWindowComponent } from './table-dialog-window.component';

describe('TableDialogWindowComponent', () => {
  let component: TableDialogWindowComponent;
  let fixture: ComponentFixture<TableDialogWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableDialogWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDialogWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
