import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-table-dialog-window',
  templateUrl: './table-dialog-window.component.html',
  styleUrls: ['./table-dialog-window.component.scss']
})
export class TableDialogWindowComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TableDialogWindowComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      console.log(this.data)
    }

  ngOnInit() {
  }



}
