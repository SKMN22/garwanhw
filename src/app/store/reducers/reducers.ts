import { TableActions, GET_BY_LOCATION_AND_PAGINATION, GET_BY_LOCATION_AND_PAGINATION_SUCCESS, GET_BY_LOCATION_AND_PAGINATION_FAILED } from '../actions';
import { createFeatureSelector } from '@ngrx/store';

export interface TableResultState{
    data:any,
    loading:boolean,
    location:string,
    pagination:number
}

export const initTableResultState : any = {
    data:{},
    location:"",
    loading:false,
    pagination:1
}

export function reducers (state = initTableResultState, action:TableActions){
    switch(action.type){
        case GET_BY_LOCATION_AND_PAGINATION:
            return{...state, loading:true,location:action.location, pagination:action.paginationNumber}
        case GET_BY_LOCATION_AND_PAGINATION_SUCCESS:
            return{...state, loading:false, data:action.payload}
        case GET_BY_LOCATION_AND_PAGINATION_FAILED:
            return{...state, loading:false}
        default:
            return{...state}
        
    }
}

export const getTableServiceReducer = createFeatureSelector<TableResultState>(
    'TABLE_STATE'
)