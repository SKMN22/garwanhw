import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from '@ngrx/effects';
import { TableService } from 'src/app/service/service';
import { of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { GET_BY_LOCATION_AND_PAGINATION, GetByLocationAndPagination, GetByLocationAndPaginationSuccess, GetByLocationAndPaginationFailed } from '../actions';

@Injectable()
export class TableResultEffects{
    constructor (private actions$: Actions, private service: TableService){}

    @Effect()
    getData$ = this.actions$.pipe(
        ofType(GET_BY_LOCATION_AND_PAGINATION),
        switchMap((action : GetByLocationAndPagination) => this.service.getByLocation(action.location,action.paginationNumber).pipe(
            map((data) => new GetByLocationAndPaginationSuccess(data)),
            catchError(err => observableOf( new GetByLocationAndPaginationFailed(err)))
        ))
    )
}