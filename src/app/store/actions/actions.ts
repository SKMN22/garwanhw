import {Action} from '@ngrx/store';

export const GET_BY_LOCATION_AND_PAGINATION = "[Table Result] Get result by location and pagination";
export const GET_BY_LOCATION_AND_PAGINATION_SUCCESS = "[Table Result] Get result by location and pagination successful";
export const GET_BY_LOCATION_AND_PAGINATION_FAILED = "[Table Result] Get result by location and pagination failed";

export class GetByLocationAndPagination{
    readonly type = GET_BY_LOCATION_AND_PAGINATION;
    constructor(public location:string, public paginationNumber: number){}
}

export class GetByLocationAndPaginationSuccess{
    readonly type = GET_BY_LOCATION_AND_PAGINATION_SUCCESS;
    constructor(public payload: any){}
}

export class GetByLocationAndPaginationFailed{
    readonly type = GET_BY_LOCATION_AND_PAGINATION_FAILED;
    constructor(public payload: any){}
}

export type TableActions = 
    GetByLocationAndPagination |
    GetByLocationAndPaginationSuccess |
    GetByLocationAndPaginationFailed;