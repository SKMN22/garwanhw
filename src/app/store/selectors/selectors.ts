import { createSelector } from '@ngrx/store';
import * as fromFeature from './../reducers/index';

export const getLoading = createSelector(
    fromFeature.getTableServiceReducer,
    (state:fromFeature.TableResultState) => state.loading
);

export const getData = createSelector(
    fromFeature.getTableServiceReducer,
    (state:fromFeature.TableResultState) => state.data
);