import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableComponentComponent } from './components/table-component/table-component.component';
import { HeaderComponent } from './components/header/header.component';
import { TableResultComponent } from './components/table-component/table-result/table-result.component';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator'
import { TableService } from './service/service';
import { HttpClientModule } from '@angular/common/http';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatSortModule } from '@angular/material';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store';
import { EffectsModule } from '@ngrx/effects';
import { TableResultEffects } from './store/effects/effects';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import { TableSpecificColumnComponent } from './components/table-component/table-result/table-specific-column/table-specific-column.component';
import { TableDialogWindowComponent } from './components/table-component/table-result/table-dialog-window/table-dialog-window.component';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    TableComponentComponent,
    HeaderComponent,
    TableResultComponent,
    TableSpecificColumnComponent,
    TableDialogWindowComponent
  ],
  entryComponents:[TableDialogWindowComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatInputModule, 
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    StoreModule.forFeature('TABLE_STATE',reducers),
    EffectsModule.forFeature([TableResultEffects]),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([])
  ],
  providers: [TableService],
  bootstrap: [AppComponent]
})
export class AppModule { }
